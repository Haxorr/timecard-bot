## Simple timecard bot that automatically logs in to UTRGV and fills out your timecard appropriately.
You MUST create your own schedule.txt and credentials.txt files. You must also have google chrome, python, and
selenium installed on your machine. To install selenium you can run the following:

`python -m pip install selenium`

After doing so to run the program you simply

`python timecard_bot.py`

Below are instructions on the two files you must create on your part.

## credentials.txt
```
sponge.bob01@utrgv.edu
password123
```

## schedule.txt
```
Mon 8:00am 10:00pm
Tue 8:00am 10:00am 1:00pm 5:00pm
Wed 8:00am 10:00pm
Thu 8:00am 10:00am 1:00pm 5:00pm
Fri 8:00am 10:00am 1:00pm 5:00pm
```

Beware sometimes you will be prompted in the terminal for an action, maximum of two times.

The file will use these and will automatically log you in. Keep in mind that you will be required to use
2 factor authentication either SMS or Authenticator. The program will pause at this stage since that can't
be automated silly.

You will be asked if you are entering your time card late (like I usually do) or not. 
Answering this question will automatically continue the program. If you responded that you are late, you will
be expected to click "prev period" by yourself because iframe is janky for trying to automate that for some reason,
might be automated in a future update. From there just respond in the terminal.

